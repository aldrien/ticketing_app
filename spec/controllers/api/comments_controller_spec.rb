require 'rails_helper'

RSpec.describe 'Comment and Authentication', :type => :request do
	describe 'should validate authentication values' do    
	    include_examples 'generate_auth_token'

	    ## Testing API

	    it 'POST #create' do
	      post url_for({ controller: 'api/comments', action: 'create_ticket_comment', params:{comment_data: {ticket_id: @ticket.id, message: Faker::Lorem.sentence}, access_token: @access_token}
	        }), xhr: true
	      parsed_response = JSON.parse(response.body)
	      expect(parsed_response['msg']).to eq('Success')	      
	    end

	    it 'PATCH #update' do
	      patch "/api/comments/#{@comment.id}",  params:{ comment_data: {message: 'new comment', id: @comment.id}, access_token: @access_token}, xhr: true
	  
	      parsed_response = JSON.parse(response.body)
	      expect(parsed_response['comment']['message']).to eq('new comment')
	      expect(parsed_response['msg']).to eq('Success')
	    end

	    it 'DELETE #destroy' do
	      delete "/api/comments/#{@comment.id}", params: {access_token: @access_token}, xhr: true
	  
	      parsed_response = JSON.parse(response.body)
	      expect(parsed_response['msg']).to eq('Success')
	    end
    end
end
