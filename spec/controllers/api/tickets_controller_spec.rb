require 'rails_helper'
include Rails.application.routes.url_helpers


RSpec.describe "Ticket and Authentication", :type => :request do  
  describe 'should validate authentication values' do    
    include_examples 'generate_auth_token'

    ## Testing API

    it 'GET #filtered_tickets' do
      get url_for({ controller: 'api/tickets', action: 'filtered_tickets', params: {keyword: @ticket.subject, access_token: @access_token} }), xhr: true        
      parsed_response = JSON.parse(response.body)
      expect(parsed_response['inprogress_ticket'][0]['subject']).to eq(@ticket.subject)
    end

    it 'GET #get_ticket_comments_and_attachments' do
      get "/api/tickets/#{@ticket.id}/get_ticket_comments_and_attachments", params: {access_token: @access_token}, xhr: true
      get url_for({ controller: 'api/tickets', action: 'get_ticket_comments_and_attachments', params:{id: @ticket.id, access_token: @access_token} }), xhr: true
      parsed_response = JSON.parse(response.body)
      expect(parsed_response['comments'][0]['message']).to eq(@comment.message)
    end

    it 'POST Create Ticket #create_or_update_ticket' do
      post url_for({ controller: 'api/tickets', action: 'create_or_update_ticket', 
        params:{
          ticket:{
            id: nil,
            subject: Faker::Lorem.word,
            description: Faker::Lorem.sentence,
            customer: @customer.id
          },
          access_token: @access_token
        }
      }), xhr: true
      
      parsed_response = JSON.parse(response.body)
      expect(parsed_response['msg']).to eq('Success')
    end

    it 'POST Update Ticket #create_or_update_ticket' do
      post url_for({controller: 'api/tickets', action: 'create_or_update_ticket', 
        params:{
          ticket:{
            id: @ticket.id,
            subject: 'new subject',
            description: Faker::Lorem.sentence,
            access_token: @access_token
          },
          access_token: @access_token
        }
        }), xhr: true
  
      parsed_response = JSON.parse(response.body)
      expect(parsed_response['ticket']['subject']).to eq('new subject')
      expect(parsed_response['msg']).to eq('Success')
    end

    it 'DELETE #destroy' do
      delete "/api/tickets/#{@ticket.id}", params: {access_token: @access_token, id: @ticket.id}, xhr: true
  
      parsed_response = JSON.parse(response.body)
      expect(parsed_response['msg']).to eq('Success')
    end

  end
end