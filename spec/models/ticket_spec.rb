require 'rails_helper'
include ActionDispatch::TestProcess

RSpec.describe Ticket, type: :model do
  # Method for checking error count/length
  shared_examples 'has_validation_error' do
    it 'should validate' do
  		ticket.valid?	 
      expect(ticket.errors[field].first).to eq(msg)
  	end 
  end

  # Test for requiring value for required fields
  %w(subject description customer_id).each do |field|
    context "validate presence of #{field}" do  	    
      	let(:ticket) {  FactoryGirl.build(:ticket, field.to_sym => nil) }
    		let(:field) { field.to_sym }
        let(:msg) { "can't be blank" }
    		include_examples 'has_validation_error'
    end
  end 

  # Test for Support's getting responsibility for a ticket
  context 'validate getting of ticket' do
  	let(:support) { FactoryGirl.create(:user, role: 2, email: Faker::Internet.email) }
  	let(:customer) { FactoryGirl.create(:user, name: Faker::Name.name, role: 1, email: Faker::Internet.email) }
  	let(:ticket) { FactoryGirl.create(:ticket, support: support, customer: customer) }

  	it 'support able to take responsibility on tickets' do
  		ticket.take_responsibility(support)
  		expect(ticket.support).to eq(support)
  		expect(ticket.status).to eq(2)
  	end
  end

  # Test for validating ticket status value
  context 'validate inclusion of status' do
  	let(:ticket) {FactoryGirl.build(:ticket, status: 4)}
  	let(:field) {:status}
    let(:msg) { "is not included in the list" }
  	include_examples 'has_validation_error'
  end

  # Test for getting ticket and ticket_attachments
  context 'get ticket and ticket_attachments' do
    let(:init_ticket) { Ticket }
    let(:customer) { FactoryGirl.create(:user) }
    let(:ticket) { FactoryGirl.create(:ticket, customer: customer) }
    let(:ticket_attachment) { FactoryGirl.create(:ticket_attachment, ticket_id: ticket.id, docu: File.new("#{Rails.root}/public/fixtures/sample.png")) }

    it 'returns json response' do
      expect( init_ticket.show_ticket_and_attachments(ticket.id)[:msg]).to eq('Success')
    end
  end  

  # Test for creating ticket & uploading attachments
  context "create ticket and it's attachments" do    
    let(:ticket) {Ticket}
    
    # Generate multiple docu
    attachments = []
    (1..3).each do
      attachments << fixture_file_upload("#{Rails.root}/spec/fixtures/sample.png", 'image/png')
    end
    let(:customer) { FactoryGirl.create(:user) }
    let(:ticket_attachments) { FactoryGirl.create(:ticket_attachment, docu: attachments) }
    let(:ticket_data) { { subject: Faker::Lorem.word, description: Faker::Lorem.sentence, ticket_attachments: attachments } }

    it 'create new ticket and upload multiple files' do 
      expect(ticket.create_or_update_ticket(ticket_data, customer)[:msg]).to eq('Success')      
    end
  end

  # Test for updating ticket
  context 'update ticket' do
    let(:init_ticket) {Ticket}
    let(:customer) { FactoryGirl.create(:user) }
    let(:ticket) { FactoryGirl.create(:ticket, customer: customer) }
    let(:new_data) { { id: ticket.id, subject: 'new subject', description: 'new description' } }
    it 'sends new ticket data for update' do
      expect(init_ticket.create_or_update_ticket(new_data, customer)[:msg]).to eq('Success')
    end
  end

  # Test for deleting ticket
  context 'delete ticket' do
    let(:init_ticket) {Ticket}
    let(:customer) { FactoryGirl.create(:user) }
    let(:ticket) { FactoryGirl.create(:ticket, customer: customer) }
    it 'deletes ticket and associated comments' do
      expect( init_ticket.destroy_ticket(ticket.id, customer)[:msg] ).to eq('Success')      
    end
  end

end