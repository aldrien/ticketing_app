require 'rails_helper'

RSpec.describe User, type: :model do
  # Method for checking user role
  shared_examples 'check role' do
  	it 'should validate' do  		
  		expect(role).to eq(true)
  	end
  end

  # Test for validating role value
  context 'validate inclusion of role' do
  	let(:user) {FactoryGirl.build(:user, role: 4)}
  	it 'return out of range error' do
  		user.valid?
  		expect(user.errors[:role].length).to eq(1)
	 end
  end

  # Test for checking role to be admin
  context 'should check if admin' do
  	let(:user) {FactoryGirl.create(:user, role: 3)}
  	let(:role) {user.is_admin?}
  	include_examples 'check role'
  end

  # Test for checking role to be support
  context 'should check if support' do
  	let(:user) {FactoryGirl.create(:user, role: 2)}
  	let(:role) {user.is_support?}
  	include_examples 'check role'
  end

  # Test for checking role to be customer
  context 'should check if customer' do
  	let(:user) {FactoryGirl.create(:user, role: 1)}
  	let(:role) {user.is_customer?}
  	include_examples 'check role'
  end

end