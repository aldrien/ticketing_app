FactoryGirl.define do
  factory :ticket_attachment do
    ticket_id 1
    docu Faker::File.file_name('foo/bar', 'test', 'doc')
  end
end
