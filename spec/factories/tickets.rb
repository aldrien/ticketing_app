FactoryGirl.define do
  factory :ticket do
    subject Faker::Lorem.word
    description Faker::Lorem.sentence
    status 1
  end
end
