require 'rails_helper'

RSpec.shared_examples 'validate_content_type' do |params|
  # Test for validating attachment content type
  let(:docu) {FactoryGirl.build(params[:model], docu: File.new("#{Rails.root}/public/favicon.ico"))}
  let(:error_msgs) { ["has contents that are not what they are reported to be", "is invalid", "content type is invalid"] }
  
  it 'returns error' do
    docu.valid?
      docu.errors[:docu].each do |error|
        expect(error_msgs).to include(error)
      end
  end
end

RSpec.shared_examples 'generate_auth_token' do
  # For user authentication
	before do
		@customer = FactoryGirl.create(:user, email: Faker::Internet.email, password: '123abc', password_confirmation: '123abc')
		@ticket = FactoryGirl.create(:ticket, subject: 'sample', description: 'test', customer_id: @customer.id)
		@comment = FactoryGirl.create(:comment, ticket_id: @ticket.id, user_id: @customer.id, status: 1)
		@comment_attachment = FactoryGirl.create(:comment_attachment, comment_id: @comment.id, docu: File.new("#{Rails.root}/public/fixtures/sample.png"))

		# Test for api authentication before accessing controller methods
		post '/api/users/signin_user', params: { email: @customer.email, password: '123abc' }, xhr: true
		@access_token = JSON.parse(response.body)['token']
	end

	it 'should have access_token' do
		expect(@access_token.blank?).to eq(false)
	end
end