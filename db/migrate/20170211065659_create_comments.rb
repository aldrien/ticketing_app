class CreateComments < ActiveRecord::Migration[5.0]
  def up
    create_table :comments do |t|
 	  t.integer :ticket_id
 	  t.integer :user_id
 	  t.text :message
 	  t.boolean :status, default: 1

      t.timestamps
    end
  end

  def down
  	drop_table :comments
  end
end
