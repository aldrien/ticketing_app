class CreateUsers < ActiveRecord::Migration[5.0]
  def up
    create_table :users do |t|
      t.string :name 
      t.string :email
      t.string :password_digest
      t.integer :role, default: 1 # customer
      t.boolean :status, default: 1
      t.string :access_token
      t.timestamps
    end
  end
  
  def down
  	drop_table :users
  end
end
