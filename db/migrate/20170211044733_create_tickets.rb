class CreateTickets < ActiveRecord::Migration[5.0]
  def up
    create_table :tickets do |t|
    	t.string :subject
    	t.text :description
 		  t.integer :customer_id
 		  t.integer :support_id
 		  t.integer :status, default: 0 #open

    	t.timestamps
    end
  end

  def down
  	drop_table :tickets
  end
end
