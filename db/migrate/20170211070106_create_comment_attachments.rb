class CreateCommentAttachments < ActiveRecord::Migration[5.0]
  def up
    create_table :comment_attachments do |t|
      t.integer :comment_id

      t.timestamps
    end
    add_attachment :comment_attachments, :docu
  end

  def down
  	drop_table :comment_attachments
    remove_attachment :comment_attachments, :docu
  end
end
