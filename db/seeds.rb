# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

# For Defaul Users
puts 'Initialize creating default ADMIN user.....'
User.create!(name: 'aldrien', email: 'aldrien@gmail.com', password: 'test123', password_confirmation: 'test123', role: 3)
puts 'Done creating default ADMIN user.....'

puts 'Initialize creating default CUSTOMER & SUPPORT user.....'
(1..3).each do |idx|
	# create customers
	User.create!(name: Faker::Name.name, email: Faker::Internet.email, password: 'test123', password_confirmation: 'test123', role: 1)

	# create supports
	User.create!(name: Faker::Name.name, email: Faker::Internet.email, password: 'test123', password_confirmation: 'test123', role: 2)
end

admins = User.get_all_admins
supports = User.get_all_supports
customers = User.get_all_customers
puts 'Initialize creating tickets.....'
# create tickets with random status
status = [0, 1, 2]
(1..5).each do |idx|	
	Ticket.create!(subject: Faker::Lorem.word, description: Faker::Lorem.sentence, support: supports.sample, customer: customers.sample, status: status.sample)
end

# create ticket open tickets
(1..5).each do |idx|
	Ticket.create!(subject: Faker::Lorem.word, description: Faker::Lorem.sentence, support: nil, customer: customers.sample, status: 0)
end
puts 'Done creating tickets.....'


# Default Comments
puts 'Initialize creating comments.....'
comments = []
(1..3).each do |idx|
	comments << {ticket_id: idx, user: supports.sample, message: Faker::Lorem.sentence}
end
Comment.create!(comments)
puts 'Done creating comments.....'

# Default TicketAttachment
puts 'Initialize creating Ticket Attachments.....'
TicketAttachment.create!(ticket_id: 1, docu: File.new("#{Rails.root}/public/fixtures/sample.png"))
puts 'Done creating Ticket Attachments.....'

# Default CommentAttachment
puts 'Initialize creating Comment Attachments.....'
CommentAttachment.create!(comment_id: 1, docu: File.new("#{Rails.root}/public/fixtures/sample.png"))
puts 'Done creating Comment Attachments.....'

puts 'Finish creating test data.'
puts "Please use ADMIN account: #{admins.sample.email} using password 'test123' "
puts "Please use CUSTOMER account: #{customers.sample.email} using password 'test123' "
puts "Please use SUPPORT account: #{supports.sample.email} using password 'test123' "
