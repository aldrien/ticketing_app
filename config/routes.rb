Rails.application.routes.draw do  
  namespace :api do
  	resources :tickets do
  		collection do
  			get :filtered_tickets
        post :create_or_update_ticket
        get :get_current_logged_in_user     
      end

  		member do
  			get :get_ticket_comments_and_attachments # ticket/1/get_ticket_comments_and_attachments        
        get :take_or_close_ticket 
  		end
    end

    resources :comments do
      collection do
        post 'create_ticket_comment' => 'comments#create_ticket_comment'
      end
    end

    resources :users do
      collection do
        get :success_login
        post :signin_user
        get :signout_user
      end
    end
  end
  
  get 'tickets/generate_report'
  get 'dashboard' => 'dashboards#index'
  
  # get 'test_data' => 'tickets#test'
  root to: 'dashboards#index'
end
