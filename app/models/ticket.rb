require 'action_view'
require 'action_view/helpers'
include ActionView::Helpers::DateHelper

class Ticket < ApplicationRecord
	default_scope { order(created_at: :desc) }

	belongs_to :customer, foreign_key: :customer_id, class_name: 'User'
	belongs_to :support, foreign_key: :support_id, class_name: 'User', optional: true
	
	has_many :ticket_attachments, dependent: :destroy
	has_many :comments, dependent: :destroy

	validates_inclusion_of :status, in: 0..2, if: 'status.present?'
	validates :customer_id, :subject, :description, presence: true

	STATUSES = ['open', 'in-progess', 'closed']

	scope :opened, -> {where(status: 0)}
	scope :in_progress, -> {where(status: 1)}
	scope :closed, -> {where(status: 2)}
	scope :closed_ticket_last_month, -> { where("tickets.created_at > ?", 1.month.ago) } #Ticket.closed_ticket_last_month

	def get_status
		STATUSES[status]
	end

	def take_responsibility(support)
		update_attributes support: support, status: 2
	end

	def close_ticket
		update_attributes status: 3
	end

	def rendered_attachments
		res = []
		ticket_attachments.each { |attachment| res << { thumb: attachment.docu.url(:thumb), original: attachment.docu.url(:original) } }
		res
	end

	class << self
		def get_ticket_comments_and_attachments(ticket_id, user)						
			res = []
			comments = nil
			cm_attachement = []

			ticket = find(ticket_id)
			
			if !user.is_admin?
				comments = ticket.comments.try(:active_comments) # shows only comment w/ status true
			else
				comments = ticket.comments # shows all
			end

			unless comments.nil?
				comments.each do |comment|
					local_res = comment.as_json
					local_res['attachments'] = comment.rendered_attachments
					local_res['commenter'] = comment.user.try(:name).try(:capitalize)
					local_res['posted_at'] = time_ago_in_words(comment.created_at.to_time)
					res << local_res
				end
			end
			{comments: res}
		end

		def show_ticket_and_attachments(id)
			ticket = find(id)
			res = ticket.as_json
			res['attachments'] = ticket.rendered_attachments

			{ticket: res, msg: 'Success', customer: ticket.customer, support: ticket.support, status: ticket.get_status}
		end

		# Use for filtering ticket result
		# params[:per] - number of records per page
		# params[:page] - number of page
		# params[:keyword] - keyword search
		# params[:status] - status
		def filtered_tickets(params, user = nil)
			res = nil
			unless user.nil?				
				where = [[]]
				where[0] << "(subject LIKE '%#{params[:keyword]}%' OR description LIKE '%#{params[:keyword]}%')" unless params[:keyword].blank?
				unless params[:status].blank?
					where[0] << 'status = ?'
					where << params[:status]
				end
				if user.is_customer?
					where[0] << 'customer_id = ?'
					where << user.id
				elsif user.is_support?
					where[0] << 'support_id IS NULL OR support_id = ?'
					where << user.id
				end
				where[0] = where[0].join(' AND ')
				res = self.where( (where.length > 1 ? where : where.first) ).page(params[:page]).per(params[:per]).order('created_at DESC')
								
				{
					open_ticket: iterate_tickets(res.opened),
					inprogress_ticket: iterate_tickets(res.in_progress),
					closed_ticket: iterate_tickets(res.closed),
					next_page: res.next_page, prev_page: res.prev_page,
					current_page: res.current_page, total_pages: res.total_pages
				}
			end
		end

		def iterate_tickets(tickets)
			data = []
			tickets.each do |ticket|
				local = ticket.as_json
				local['formatted_status'] = ticket.get_status
				local['support'] = ticket.support
				local['customer'] = ticket.customer
				data << local
			end
			data	
		end

		# Create or Update Ticket
		def create_or_update_ticket(params, user)
			msg = ''
			ticket = nil

			if params[:id].blank?
				begin
					ticket = self.create!(
						subject: params[:subject], description: params[:description], customer_id: user.id, support_id: params[:support_id], status: 0)
					unless params[:ticket_attachments].nil?
						attachments = []
						params[:ticket_attachments].each do |k, attachment|
							ticket.ticket_attachments.create!(docu: attachment)
						end
						# single upload
						# ticket.ticket_attachments.create!(docu: params[:ticket_attachments])
					end
					msg = 'Success'
				rescue => e
					msg = "Error unable to create ticket: #{e.message}."
				end
			else
				begin
					ticket = find(params[:id])
					if user.is_admin? || (ticket.customer == user)
						support_id = params[:support_id]
						support_id = nil if params[:support_id].to_i == 0

						msg = 'Success' if ticket.update_attributes({
							subject: params[:subject], description: params[:description], status: params[:status], support_id: support_id  })
					end
				rescue => e
					msg = "Error unable to find ticket: #{e.message}."
				end
			end

			{ticket: ticket, msg: msg}
		end
		
		# Destroy
		def destroy_ticket(id, user)
			msg = ''
			begin
				ticket = find(id)
				if user.is_admin? || (ticket.customer == user)
					msg = 'Success' if ticket.destroy
				end
			rescue => e
				msg = "Error unable to find ticket: #{e.message}."
			end
			{msg: msg}
		end

		# Support User take Ticket
		# status = in_progress or close
		def take_or_close_ticket(id, do_action, user)
			msg = ''
			begin
				ticket = find(id)
				if do_action == 'take'
					set_status = 1					
				else # close
					set_status = 2
				end
				msg = 'Success' if ticket.update_attributes({status: set_status, support_id: user.id})
			rescue => e
				msg = "Error: #{e.message}."
			end
			{msg: msg, status: set_status}
		end
	end
end