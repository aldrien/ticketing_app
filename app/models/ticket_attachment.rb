class TicketAttachment < ApplicationRecord
	belongs_to :ticket

	has_attached_file :docu, :styles => {thumb: '50x50>'}

	# Checks for attachment content type
	validates_attachment :docu, 
	:content_type => {
		:content_type => %w(image/jpeg image/jpg image/png)
	}

	# For further development
	 # application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document
end
