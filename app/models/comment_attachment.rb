class CommentAttachment < ApplicationRecord
	belongs_to :comment
	
	has_attached_file :docu, :styles => {thumb: '50x50>'}

	# Checks for attachment content type
	validates_attachment :docu, 
	:content_type => {
		:content_type => %w(image/jpeg image/jpg image/png)
	}
end
