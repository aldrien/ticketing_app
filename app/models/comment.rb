class Comment < ApplicationRecord
	default_scope { order(created_at: :desc) }
	belongs_to :ticket
	belongs_to :user
	has_many :comment_attachments, dependent: :destroy
	scope :active_comments, -> { where(status: true) }

	def rendered_attachments
		res = []
		comment_attachments.each { |attachment| res << { thumb: attachment.docu.url(:thumb), original: attachment.docu.url(:original) } }
		res
	end

	class << self
		# Create
		def create_comment(params, user)
			msg = ''
			comment = nil
			begin
				comment = self.create!(ticket_id: params[:ticket_id], user_id: user.id, message: params[:message])
				unless params[:comment_attachments].nil?
					attachments = []
					params[:comment_attachments].each { |k, v| attachments << {docu: v, comment: comment} }
					comment.comment_attachments << CommentAttachment.create!(attachments)
				end
				msg = 'Success'
			rescue => e
				msg = "Error unable to add comment: #{e.message}."
			end
			{comment: comment, msg: msg}
		end

		# Update
		def update_comment(params, user)
			msg = ''
			comment = nil
			begin
				comment = find(params[:id])
				if user.is_admin? || (comment.user == user)
					msg = 'Success' if comment.update_attributes({ message: params[:message] })
				end
			rescue => e
				msg = "Error unable to find comment: #{e.message}."
			end
			{comment: comment, msg: msg}
		end

		# Destroy
		def destroy_comment(id, user)
			msg = ''
			begin
				comment = find(id)
				if user.is_admin? || (comment.user == user)
					msg = 'Success' if comment.update_attribute :status, false
				end
				msg = 'Success'
			rescue => e
				msg = "Error unable to find comment: #{e.message}."
			end
			{msg: msg}
		end
	end
end