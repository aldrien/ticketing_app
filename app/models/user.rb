require 'bcrypt'

# Email format validator class
class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      record.errors[attribute] << (options[:message] || "is invalid format")
    end
  end
end

class User < ApplicationRecord
	has_secure_password

	has_many :customer_tickets, foreign_key: :customer_id, class_name: 'Ticket', dependent: :destroy
	has_many :support_tickets, foreign_key: :support_id, class_name: 'Ticket', dependent: :destroy

	validates_uniqueness_of :name, presence: true
	validates_uniqueness_of :email, presence: true, email: true
	validates :password, :length => { minimum: 5, message: 'is too short, minimum of 6 characters.' }, on: :create
	validates_inclusion_of :role, :in => 1..3

	before_save :set_downcase_email

	scope :get_all_admins, -> { where(role: 3) }
	scope :get_all_supports, -> { where(role: 2) }
	scope :get_all_customers, -> { where(role: 1) }

	before_create :set_access_token

	def is_admin?
		role == 3
	end

	def is_support?
		role == 2
	end

	def is_customer?
		role == 1
	end

	# set email to lower case before save
	def set_downcase_email
		self.email = email.downcase if email.present?    
	end

	class << self
		# Show
		def show_user(id)
			msg = ''
			user = nil
			begin
				user =  find(id)
				msg = 'Success'
			rescue => e
				msg = "Record not found: #{e.message}."
			end
			{user: user, msg: msg}
		end

		# Create
		def create_user(params)
			msg = ''
			user = nil
			begin
				user = self.create!(name: params[:name], email: params[:email], password: params[:password], password_confirmation: params[:password_confirmation])
				msg = 'Success'
			rescue => e
				msg = "#{e.message}."
			end
			{user: user, msg: msg}
		end

		# Update
		def update_user(id, params, current_user)
			msg = ''
			user = nil
			begin
				user = find(id)
				if current_user.is_admin? || (current_user.email == user.email)
					msg = 'Success' if user.update_attributes!({
						name: params[:name],
						email: params[:email],
						password: params[:password],
						password_confirmation: params[:password_confirmation],
						status: params[:status],
						role: params[:role]
					}.reject{|k,v| v.blank?})					
				else
					msg = 'Forbidden access! Not allowed!'
				end
			rescue => e
				msg = e.message
			end
			{user: user,  msg: msg, test: params[:role]}
		end

		# Destroy
		def destroy_user(id, current_user)
			msg = ''
			if current_user.is_admin?
				begin
					user = find(id)
					msg = 'Success' if user.destroy
				rescue => e
					msg = e.message
				end
			else
				msg = 'Forbidden access! Not allowed!'
			end
			{msg: msg}
		end
	end

	private

	def set_access_token
		self.access_token = BCrypt::Password.create("#{email} + #{Time.now.utc}")
	end
end