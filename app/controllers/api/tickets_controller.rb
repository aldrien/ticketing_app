class Api::TicketsController < ApplicationController
  	before_action :authenticate_user!

	def filtered_tickets
		render json: Ticket.filtered_tickets(params, @current_user)
	end

	def get_ticket_comments_and_attachments
		render json: Ticket.get_ticket_comments_and_attachments(params[:id], @current_user)
	end

	def take_or_close_ticket
		render json: Ticket.take_or_close_ticket(params[:id], params[:do_action], @current_user)
	end

	def show
		render json: Ticket.show_ticket_and_attachments(params[:id])
	end	

	def create_or_update_ticket
		render json: Ticket.create_or_update_ticket(params[:ticket], @current_user)
	end

	def destroy
		render json: Ticket.destroy_ticket(params[:id], @current_user)
	end

	# Ticket PDF Report Generation
	def generate_pdf
	  	@report_title = 'Ticket-Report'
	    template = 'tickets/ticket_report.pdf.erb'
	    
		respond_to do |format|
			format.html
			format.pdf do
				render title:         @report_title,
				pdf:           @report_title,
				template:      template,
				layout:        'pdf_template.html',
				footer: {
				  center:    '[page] of [topage]',
				  right:     '',
				  font_name: 'Arial',
				  font_size: 8
				}
			end
		end
	end
end