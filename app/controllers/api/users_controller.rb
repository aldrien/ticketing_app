class Api::UsersController < ApplicationController
  before_action :authenticate_user!, except: [:signin_user, :signout_user]
  
  def index
    render json: {
      admins: User.get_all_admins,
      supports: User.get_all_supports,
      customers: User.get_all_customers
    }
  end

  def create
  	render json: User.create_user(params)
  end

  def show
    render json: User.show_user(params[:id])
  end
  
  def update
    render json:  User.update_user(params[:id], params[:user], @current_user)
  end

  def destroy
    render json: User.destroy_user(params[:id], @current_user)
  end

  def signin_user
    msg = nil
    begin
      @user = User.where(email: params[:email], status: true).first.try(:authenticate, params[:password])
      session[:access_token] = @user.access_token
      render json: {token: @user.access_token, name: @user.name.capitalize, id: @user.id, msg: 'Success', role: @user.role}
    rescue
      render json: {token: nil, msg: 'Invalid email or password.'} 
    end
  end

   # Logs out the current user
  def signout_user
    session.delete(:access_token)
    @current_user = nil
    render json: {msg: 'Success'}
  end

  def success_login
    render json: {
      data: {
        message: "Welcome #{@current_user.name}",
        user: @current_user
      }
    }, status: 200
  end

end