class Api::CommentsController < ApplicationController
	before_action :authenticate_user!

	def create_ticket_comment
		render json:  Comment.create_comment(params[:comment_data], @current_user)
	end

	def update
		render json: Comment.update_comment(params[:comment_data], @current_user)		
	end

	def destroy
		render json: Comment.destroy_comment(params[:id], @current_user)	  
	end
end
