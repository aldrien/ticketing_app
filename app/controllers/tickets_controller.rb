class TicketsController < ApplicationController
  before_action :authenticate_user!

  # Ticket PDF Report Generation
  def generate_report
  	@closed_ticket = Ticket.where(support_id: @current_user.id).closed_ticket_last_month
  	@dateTimeNow = Time.new.strftime("%a, %e %b %Y %H:%M")

  	@report_title = 'Closed-Ticket-Report'
    template = 'tickets/ticket_report.pdf.erb'
    
	respond_to do |format|
		format.html
		format.pdf do
			render title:         @report_title,
			pdf:           @report_title,
			template:      template,
			layout:        'pdf_template.html',
			footer: {
			  center:    '[page] of [topage]',
			  right:     '',
			  font_name: 'Arial',
			  font_size: 8
			}
		end
	end
  end

end
