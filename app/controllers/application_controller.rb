class ApplicationController < ActionController::Base  
  # protect_from_forgery prepend: true # with: :exception
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format.include? 'application/json' }
  
  before_action :set_page_per

  private
    # setting default values for pagination
    # this will also prevent dev accidentally sending string to api instead of number
    def set_page_per
    	params[:page] ||= 1
    	params[:per] ||= 20
    	params[:page] = (params[:page].to_i > 0) ? params[:page].to_i : 1
    	params[:per] = (params[:per].to_i > 0) ? params[:per].to_i : 20
    end

    def json_request?
      request.format.json?
    end

    def authenticate_user!
      begin
        @current_user = User.where(access_token: params[:access_token]).first        
      rescue
        raise 'Unauthorize access!'
      end
    end

end
