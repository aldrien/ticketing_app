// User Login
app.factory('Auth', function($resource){
	return $resource('/api/users/:action', {action: '@action'}, {
		signin: { method: 'POST', isArray: false, params: { action: 'signin_user' } },
		signout: { method: 'GET', isArray: false, params: { action: 'signout_user' } }
	})
})

// User Registration
app.factory('RegisterUser', function($resource){
	return $resource('/api/users/:action', {action: '@action'}, {
		signUp: { method: 'POST', isArray: false, params: { action: '' } }		
	})
})

// Ticket showing and management
app.factory('Ticket', function($resource){
	return $resource('/api/tickets/:id/:action', {action: '@action', id: '@id'}, {
		get_all_tickets: { method: 'GET',  params: { action: 'filtered_tickets' } },
		show_ticket: { method: 'GET', isArray: false,  params: { action: '' } },
        create_or_update_ticket: { method: 'POST', isArray: false,  params: { action: 'create_or_update_ticket' } },
        delete_ticket: { method: 'DELETE', isArray: false,  params: { action: '' } },
        get_all_ticket_comments: { method: 'GET', isArray: false,  params: { action: 'get_ticket_comments_and_attachments' } },
        take_or_close_ticket: { method: 'GET', isArray: false, params: { action: 'take_or_close_ticket' } }
	})
})

// Comment posting
app.factory('Comment', function($resource){
	return $resource('/api/comments/:id/:action', {action: '@action', id: '@id'}, {
        create_comment: { method: 'POST', isArray: false,  params: {action: 'create_ticket_comment'} }
	})
})

// Users management
app.factory('User', function($resource){
	return $resource('/api/users/:id/:action', {action: '@action', id: '@id'}, {
		get_all_users: { method: 'GET', isArray: false,  params: { action: '' } },
		get_user: { method: 'GET', isArray: false,  params: { action: '' } },
		update_user: { method: 'PATCH', isArray: false,  params: { action: ''} },
		delete_user: { method: 'DELETE', isArray: false,  params: { action: ''} }
	})
})

// Use for Inline IF Statement
app.filter('iif', function () {
   return function(input, trueValue, falseValue) {
        return input ? trueValue : falseValue;
   };
});
