var app = angular.module('app', ['ui.router', 'templates', 'ngResource', 'ngFileUpload']).config([
    '$stateProvider',
    '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('sign_in', {
                url: '/sign_in',
                templateUrl: '/angular_views/sign_in.html',
                controller: 'SignInController'
            })

            .state('sign_up', {
                url: '/sign_up',
                templateUrl: '/angular_views/sign_up.html',
                controller: 'SignUpController'
            })

            .state('dashboard', {
                url: '/dashboard',
                templateUrl: '/angular_views/dashboard.html',
                controller: 'DashboardController'
            })

            .state('users', {
                url: '/dashboard/users',
                templateUrl: '/angular_views/users.html',
                controller: 'UsersController'
            })

            .state('user_show', {
                url: '/dashboard/user_show/:id',
                templateUrl: '/angular_views/user_show.html',
                controller: 'UsersController'
            })

            .state('show_ticket', {
                url: '/dashboard/show_ticket/:id',
                templateUrl: '/angular_views/show_ticket.html',
                controller: 'ShowTicketController'
            })

        $urlRouterProvider.otherwise('sign_in');
    }
])

app.factory('methodFactory', function () {
    return { setNav: function () {
        var $nav = $('#header_nav');
        
        $nav.removeClass('navigator_hide navigator_show')
        
        if(TOKEN.length > 0){
            $nav.addClass('navigator_show')
        }else{
            $nav.addClass('navigator_hide')
        }
    }}
})