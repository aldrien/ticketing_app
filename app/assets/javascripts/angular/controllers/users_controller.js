app.controller('UsersController', function($scope, $window, methodFactory, $state, User){	
	if(TOKEN.length <= 0){
		$state.go('sign_in')
	}

	$scope.currentUserRole = $window.sessionStorage.getItem('currentUserRole')
    $scope.currentUserId = $window.sessionStorage.getItem("currentUserID")
	$scope.getToken = TOKEN
    
	methodFactory.setNav()

	if($scope.currentUserRole == 3)
		$('li#goToUsers').css({'display':'flex'})
	
	// If NOT Admin User
	if($window.sessionStorage.getItem('currentUserRole') != 3){
		window.location.href = '/#!/sign_in'
	}

	// Showing Logged-In User's Name 
	$('div.currentUserName').text("Hi " + $window.sessionStorage.getItem("currentUserName") + " !")

	$scope.error_message = ''
	$scope.info_message = ''

	// User List
	$scope.get_all_users = function(){
		User.get_all_users({}, function(res){
			$scope.json_users = res
		});
	}
	$scope.get_all_users()

	// User Show	
	$scope.userData = {		
		name: '',
		email: '',
		password: '',
		password_confirmation: '',
		status: '',
		role: '',
		access_token: TOKEN
	}
	$scope.show_user = function(){
		User.get_user({id: $state.params['id'], access_token: TOKEN}, function(res){
			if(res['msg'] == 'Success'){
				$scope.userData = {
					name: res['user']['name'],
					email: res['user']['email'],
					status: res['user']['status'].toString(),
					role: res['user']['role'],
					access_token: res['user']['access_token']
				}
				$scope.availableOptions = [{name: 'Activated', id: true}, {name: 'Deactivated', id: false}]
			}else{
				$scope.error_message = res['msg']
			}
		})
	}
	$scope.show_user();

	// User Update
	$scope.do_update_user = function(){		
		if($scope.userData.password != $scope.userData.password_confirmation){
			$scope.error_message = 'Password and Password-Confirmation does not matched!'
			return false
		}
		User.update_user({access_token: TOKEN, user: $scope.userData, id: $state.params['id']}, function(res){			
			if(res['msg'] == 'Success'){
				$scope.error_message = ''
				$scope.info_message = 'User data was successfully updated.'				
				// $state.go('users')
			}else{
				$scope.info_message = ''
				$scope.error_message = res['msg']
			}
		});
	}

	// On-click delete button, pass ticket_id
    $scope.target_user_id = null
    $scope.setUserId = function(id) {
    	$scope.target_user_id = id
    }


	// User Update
	$scope.deleteUser = function(id){
		User.delete_user({access_token: TOKEN, id: id}, function(res){			
			if(res['msg'] == 'Success'){
				$scope.info_message = 'User account was successfully deleted.'
				jQuery('.modal').modal('hide')			
				$scope.get_all_users();
			}else{
				$scope.error_message = res['msg']
			}
		})
	}

	// Use for Bootstrap Nav Tabs
	angular.element(document).ready(function(){
		$('.nav-tabs li a').on('click', function(e){
			e.preventDefault();
			$(this).tab('show');
		});
	})
	
})