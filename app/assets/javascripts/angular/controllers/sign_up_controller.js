app.controller('SignUpController', function($scope, methodFactory, $state, RegisterUser){
	methodFactory.setNav()
	
	$scope.signUp = {
		name: '',
		email: '',
		password: '',
		password_confirmation: ''
	}

	$scope.error_message = ''

	$scope.do_sign_up = function(){
		RegisterUser.signUp($scope.signUp, function(res){
			if(res['msg'] == 'Success'){
				$state.go('sign_in')
			}else{
				$scope.error_message = res['msg']
				// ng-bind-html="error_messages"
				// $scope.error_messages.trustAsHtml(res['msg'].toString().replace(/,/g, '<br>'))
			}
		})
	}
})