app.controller('DashboardController', function($scope, $window, Upload, methodFactory, $state, Ticket, User){
	if(TOKEN.length <= 0){
		$state.go('sign_in')
	}

	$scope.currentUserRole = $window.sessionStorage.getItem('currentUserRole')
    $scope.currentUserId = $window.sessionStorage.getItem("currentUserID")
    $scope.getToken = TOKEN
    
    if($scope.currentUserRole == 3){
        $('li#goToUsers').show()
    }else{
        $('li#goToUsers').hide()
    }

	methodFactory.setNav()

    // Showing Logged-In User's Name 
	$('div.currentUserName').text("Hi " + $window.sessionStorage.getItem("currentUserName") + " !")
    
	// Variable declarations
	$scope.info_message = ''	

	$scope.ticket_params = {
		page: 1,
		per: 20,
		keyword: null,
        access_token: TOKEN
	}

    $scope.tickets = []
    $scope.open_tickets = []
    $scope.inprogress_tickets = []
    $scope.closed_tickets = []

    // Getting All Tickets by status
	$scope.get_filtered_tickets = function(){
	    Ticket.get_all_tickets($scope.ticket_params, function(res){			
			$scope.open_tickets = res.open_ticket
		    $scope.inprogress_tickets = res.inprogress_ticket
    		$scope.closed_tickets = res.closed_ticket
		});
	}
	$scope.get_filtered_tickets()

	// Creating or Updating Ticket
    // Upload Attachments
    $scope.ticket = {}
    $scope.createOrUpdateTicket = function () {
        Upload.upload({
            url: '/api/tickets/create_or_update_ticket',
            data: { ticket: $scope.ticket, access_token: TOKEN }
        }).then(function (resp) {
            $('#ticketAttachmentFileToBeUploaded').text('')
            jQuery('.modal').modal('hide')

            if(resp['data']['msg'] == 'Success'){
                $scope.ticket = {}
                $scope.get_filtered_tickets()
            }else{
                $scope.info_message = resp['msg']
            }
        }, function (resp) {
            alert('Error uploading .....')
        }, function (evt) {});
    }
    
    $scope.previewImageNames = function(files){
        file_names = ''
        $.each(files, function(k, v){
            file_names  += v['name'] + ', '
        })
        $('#ticketAttachmentFileToBeUploaded').text(file_names)        
    }

    // When Update button clicked
    $scope.editTicket = function(id){
        Ticket.show_ticket({id: id}, function(res){            
            $scope.ticket = res.ticket
        });
    }

    // Support Taking Ticket
    $scope.takeTicket = function(id){    	
    	Ticket.take_or_close_ticket({access_token: TOKEN, id: id, do_action: 'take'}, function(res){
    		if(res['msg'] == 'Success'){
    			$scope.get_filtered_tickets()
    		}
    		$scope.info_message = res['msg']
    	})
    }

    // Getting All Support Users
    $scope.getSupportUsers = function(){
    	User.get_all_users({}, function(res){
			$scope.json_support_users = res['supports']			
		});
    }
    $scope.getSupportUsers();


    // On-click delete button, pass ticket_id
    $scope.target_ticket_id = null
    $scope.setTicketId = function(id) {
    	$scope.target_ticket_id = id
    }

    // Deleting Ticket (ONLY ADMIN USER)
    $scope.deleteTicket = function(id){
    	Ticket.delete_ticket({access_token: TOKEN, id: id}, function(res){
    		if(res['msg'] == 'Success'){	    		
	    		$scope.get_filtered_tickets()
	    	}
	    	jQuery('.modal').modal('hide')
	    	$scope.info_message = res['msg']
    	})
    }

    // Show Users
    $scope.showUsers = function(){
        $state.go('users')
    }
   
    // Use for Bootstrap Nav Tabs
	angular.element(document).ready(function(){
	    $('.nav-tabs li a').on('click', function(e){
	        e.preventDefault();
	        $(this).tab('show');
	    });
	})

})