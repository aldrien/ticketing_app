app.controller('SignInController', function($scope, $window, methodFactory, $state, Auth){
	methodFactory.setNav()

	// Variable declarations
	$scope.signin_data = {
		email: '',
		password: '',
		access_token: TOKEN
	}
	$scope.error_message = ''

	// Check if Token exists
	if(TOKEN.length > 0){
		$state.go('dashboard')
	}

	// User SignIn
	$scope.do_signin = function(){		
		Auth.signin($scope.signin_data, function(res){
			if(res['msg'] == 'Success'){
				TOKEN = res['token']
				methodFactory.setNav()
				
				//for angular views
				$window.sessionStorage.setItem("currentUserRole", res['role'])
				$window.sessionStorage.setItem("currentUserName", res['name'])
				$window.sessionStorage.setItem("currentUserID", res['id'])
				
				$state.go('dashboard')
			}else{
				$scope.error_message = (res['msg'])
			}
		})
	}

	// User SignOut
    $scope.do_signout = function(){
        Auth.signout({}, function(res){
            // Clearing session storage
            $window.sessionStorage.setItem("currentUserRole", null)
            $window.sessionStorage.setItem("currentUserName", null)
            $window.sessionStorage.setItem("currentUserID", null)

            window.location.href = '/'
        })
    }
})