app.controller('ShowTicketController', function($scope, $window, Upload, methodFactory, $state, Ticket, Comment){
	if(TOKEN.length <= 0){
		$state.go('sign_in')
	}

	$scope.currentUserId = $window.sessionStorage.getItem("currentUserID")
	$scope.currentUserRole = $window.sessionStorage.getItem("currentUserRole")

	methodFactory.setNav()
	
	$scope.error_messages = ''
	$scope.commentData = {		
		ticket_id: $state.params['id'],
		message: ''
	}

	$scope.appUrl = window.location.origin

	$scope.json_ticket = {} 
	// Show Ticket Data for viewing & posting comment
	$scope.show_ticket_data = function(){
		Ticket.show_ticket({id: $state.params['id']}, function(res){			
			$scope.json_ticket = res
		});
	}
	$scope.show_ticket_data()
	
	// Get and show comments
	$scope.show_ticket_comments = function(){
		Ticket.get_all_ticket_comments({access_token: TOKEN,  id: $state.params['id']}, function(res){
			$scope.ticket_comments = res['comments']			
		});
	}
	$scope.show_ticket_comments()

	// Post Comment
	$scope.post_comment = function(){
		Comment.create_comment({access_token: TOKEN, comment_data: $scope.commentData}, function(res){
			if(res['msg'] == 'Success'){
				$scope.show_ticket_comments()
				$("textarea[name='message']").val('')
			}else{
				$scope.error_messages = res['msg']
			}
		})
	}

	// Support Taking Ticket
	$scope.takeTicket = function(id){
		Ticket.take_or_close_ticket({access_token: TOKEN, id: id, do_action: 'take'}, function(res){
			$state.go('dashboard')
		})    	
	}

    // Support Closing Ticket
    $scope.closeTicket = function(id){
    	Ticket.take_or_close_ticket({access_token: TOKEN, id: id, do_action: 'close'}, function(res){
    		$state.go('dashboard')
    	})    	
    }

    $scope.comment = {
    	ticket_id: $state.params.id
    }
    $scope.sendComment = function () {
    	Upload.upload({
    		url: '/api/comments/create_ticket_comment',
    		data: { comment_data: $scope.comment, access_token: TOKEN }
    	}).then(function (resp) {
    		$('#commentAttachmentFileToBeUploaded').text('')
    		$("textarea[name='message']").val('')
    		$scope.show_ticket_comments()
    		// console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
    	}, function (resp) {
    		// console.log('Error status: ' + resp.status);    		
    	}, function (evt) {
    		// var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
    		// console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
    	});
    };

    $scope.previewImageNames = function(files){
    	file_names = ''
    	$.each(files, function(k, v){
    		file_names	+= v['name'] + ', '
    	})
    	$('#commentAttachmentFileToBeUploaded').text(file_names)    	
    }

    $scope.setTicketId = function(id) {
    	$scope.comment.ticket_id = id
    }

})